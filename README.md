# Hello API

[![pipeline status](https://gitlab.com/gibbles/hello-api/badges/master/pipeline.svg)](https://gitlab.com/gibbles/hello-api/commits/master)

A simple ReST API that provides a little twist on "hello world".

When accessing the root resource, you get the well-trodden "hello" message as a json response, however, there is a "songs" resource that returns "hello" song objects: the full list at `/songs/` and individual songs with a resource id like `/songs/1`.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To run this api after a git pull, you miminally need __docker__ and __make__.

However, the application is written in __nodejs__, so to develop on the application you'll also need to have it installed.

I'm a fan of [Visual Studio Code](https://code.visualstudio.com/). Among the files are some vs code-specific settings, but you aren't limited to that editor.

### Installing

#### Run in a container

To simply run the application in a container, follow these steps:

```sh
make build
make run
```

The application should now be listening. To test, open a new terminal and type:

```sh
make curl200
make curl400
make curlhealth
```

Give your original shell a Ctrl-C to kill the running container when you're done with manual testing.

#### Run on your machine

You'll likely want to skip the docker build step if you're iterating quickly on some changes. There are a handful of make rules (as well as npm scripts) to help you here as well.

```sh
npm install
make test
make start
```

The `start` make rule will use `nodemon` to keep the api running, and restart it if you change any files.

You can now use the same `make curlxxx` rules to test the running program like you did with the docker approach.

Just like with the docker method of running, you can use Ctrl-C to stop the program.

## Running the tests

There are several test rules in the Makefile.

* `make test` runs the commit tests, including linting and unit tests with coverage. The output of the tests are stored in the `reports` folder.
* `make smoke` runs the smoke tests. Currently they are just placeholders.
* `make acceptance` runs the acceptance tests. These are also just placeholders for now.

### And coding style tests

This project uses the [airbnb eslint ruleset for javascript](https://github.com/airbnb/javascript). It is no joke and will make you sad, but your code will be tight.

The linter runs automatically as part of the `make test` rule. However, you can run the linter independently with it's own rule.

```sh
make lint
```

Builds fail if you do not pass linting.

## Deployment

To deploy, make sure the pipeline has completed successfully on the master branch, then go to [tags](https://gitlab.com/gibbles/hello-api/-/tags) and add a new tag, being sure to fill out the release notes.

Once you save the tag, a new pipeline will run and deploy the tagged commit (which should already have been built via a previous pipeline) to the production namespace of your kubernetes cluster.

### GKE

To deploy to your GKE cluster, you'll need to have [helm installed properly, and with the correct permissions](https://helm.sh/docs/using_helm/).

**Note**: GitLab has an option to set up Helm Tiller on your cluster, *but it is only set up to deploy the GitLab applications*, so even if it is installed, you have to install your own tiller as well.

You also need to have an [ingress controller installed](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) correctly in your cluster to access your application from the public internet.

#### Automated Deployment

The deployment scripts assume deployment to a Google Kubernetes Engine cluster. Therefore, you'll need several environment variables set for the deployment to work from the CI tool.

| Variable | Notes |
|---|---|
| GKE_CLUSTER_NAME | Find it on the list of Kubernetes clusters in gke. |
| GKE_PROJECT | Use the project id, not project name. |
| GKE_SERVICE_ACCOUNT | The base64-encoded string of the json file produced for a service account key in GCP IAM see [how to gke]. |
| GKE_ZONE | e.g. us-central1-a. |
| STAGE | The stage of the build. [dev, qa, prod, etc.] |
| APP_VERSION | This will be used as the docker image tag the deployment will use. Semantic versioning here. |

#### Local Deployment

You can also run `make deploy` locally, and your application will be deployed as a release with your username as the suffix. However, you'll need to have helm initialized locally for this to work. This allows you to bypass all of the GKE-prefixed environment variables. Also, STAGE and APP_VERSION will be defaulted for a local deployment.

If helm is set initialized, you just need to execute a simple make command to deploy the application to kubernetes.

```sh
make deploy
```

#### Domain Name Caveats

Though it is not required, you can set up a wildcard dns record if you have a domain laying around. You should assign the record to point to the IP address of your ingress controller.

If you don't set up dns, then you'll need to pass the `Host` header when you access the api from the public internet so that the ingress controller will know how to route you. For example,

```sh
curl --header 'Host: hello-api.dshaneg.com' http://xxx.xxx.xxx.xxx/songs/1
```

## Built With

* [expressjs](https://expressjs.com/) - Web framework
* [bunyan](https://github.com/trentm/node-bunyan) - Logging framework
* [npm](https://www.npmjs.com/) - Dependency Management

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/gibbles/hello-api/-/tags).

## Authors

* [**Shane Gibbons**](https://gitlab.com/dshaneg) - *Initial work*

See also the list of [contributors](https://gitlab.com/gibbles/hello-api/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

I got tons of mileage from the ideas at the following links. Thanks!

* [Docker and Node.js Best Practices Guide](https://github.com/nodejs/docker-node/blob/master/docs/BestPractices.md)
* [GitLab docker CI](https://blog.callr.tech/building-docker-images-with-gitlab-ci-best-practices/)
* [CacheFrom Option](https://medium.com/@gajus/making-docker-in-docker-builds-x2-faster-using-docker-cache-from-option-c01febd8ef84)
* [Open Container Initiative Image Format Specification](https://github.com/opencontainers/image-spec/blob/master/annotations.md) for `LABEL`s
* [Set up gitlab for deploying helm charts to gke][how to gke]
* [README template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)

[how to gke]: https://medium.com/@yanick.witschi/automated-kubernetes-deployments-with-gitlab-helm-and-traefik-4e54bec47dcf
