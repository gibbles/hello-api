#! /bin/bash

source abin/ensure-version-vars.sh

helm init --client-only

helm lint chart/hello-api

helm package --version ${CHART_VERSION} --app-version ${APP_VERSION} chart/hello-api
